package ru.bokhan.tm.api.service;

import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User updateById(
            String id, String login,
            String firstName, String lastName, String middleName,
            String email
    );

    User updatePasswordById(String id, String password);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

    User removeUserByLogin(String login);

}

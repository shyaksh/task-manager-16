package ru.bokhan.tm.api.service;

import ru.bokhan.tm.enumerated.Role;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void checkRoles(Role[] roles);

    boolean isAuth();

    boolean isAdmin();

    void logout();

    void registry(String login, String password, String email);

}

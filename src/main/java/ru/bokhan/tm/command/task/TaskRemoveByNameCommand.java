package ru.bokhan.tm.command.task;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Task;
import ru.bokhan.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}

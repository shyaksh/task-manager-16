package ru.bokhan.tm.command.user;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.User;

public class ProfileViewCommand extends AbstractCommand {

    @Override
    public String name() {
        return "profile-view";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Show my profile";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROFILE]");
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
    }

}

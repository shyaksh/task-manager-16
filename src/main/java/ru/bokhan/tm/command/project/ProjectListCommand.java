package ru.bokhan.tm.command.project;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Project;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (final Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}
